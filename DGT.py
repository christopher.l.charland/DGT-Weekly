"""
Python implementation of the DGT Weekly. Just for funsiez

"""

import argparse, asyncio, logging, json

class flagServerProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        peername = transport.get_extra_info('peername')
        logger.info('Connection recieved from %s', peername)
        self.transport = transport

    def data_received(self, data):
        message = data.decode()
        print('Data received')
        try:
            unJsonMsg = json.loads(message)
            self.transport.write('valid\n'.encode())
            print(unJsonMsg)
        except:
            self.transport.write('I couldnt decode that\n'.encode())
            print(message)



if __name__ == '__main__':
    parse = argparse.ArgumentParser(description='DGT Weekly CTF python/asyncio.')
    parse.add_argument('-p', '--port', help='Port to listen on for flag submissions', required=True)
    parse.add_argument('-d', '--db-port', help='Port to connect to database')
    parse.add_argument('-i', '--db-ip', help='IP to connect to the database')
    args = parse.parse_args()


    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s:%(message)s"
        )
    logger = logging.getLogger('DGTLogger')

    loop = asyncio.get_event_loop()
    whatfor = loop.create_server(flagServerProtocol, '0.0.0.0', args.port)
    server = loop. run_until_complete(whatfor)

    logging.info('Started server on {}'.format(server.sockets[0].getsockname()))

    try:
        loop.run_forever()

    except KeyboardInterrupt:
        pass
    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()